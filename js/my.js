$('.price_list_item').each(function(){
    var barva = $(this).attr('data-barva')
    var pozadi = $(this).attr('data-pozadi')
    var barvaPozadi = $(this).attr('data-barvaPozadi')

    $(this).find(".price_list_item_header").css("background-color",barva)
    $(this).find(".bullet").css("background-color",barva)
    $(this).find("button").css("color",barva)
    $(this).find("button").css("border-color",barva)
    

    if(pozadi == "yes") {
        $(this).find(".price_list_item_body").css("background-color", barvaPozadi)
    }

})


//video change

//.click(function(){})



$('.video_thumbnail').click(function(){
    // get new video data
    var new_video = $(this).attr('data-video');
    var new_image = $(this).find('.video_preview').attr('style');
    var new_text =  $(this).find('p').text();

    // get old video data
    var old_video = $('.main_video').attr('href');
    var old_image = $('.main_video').attr('style');
    var old_text =  $('.main_video').attr('data-title');

    // isnert new to old
    $('.main_video').attr('href', new_video)
    $('.main_video').attr('style', new_image)
    $('.main_video').attr('data-title', new_text)


    // inser old to new`
    $(this).attr('data-video', old_video);
    $(this).find('.video_preview').attr('style', old_image)
    $(this).find('p').text(old_text)

})

$(document).ready(function(){


    




const recenze_slider = new Swiper('.recenze_slider', {
    loop: true,
    breakpoints: {
        0: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        768: {
          slidesPerView: 2,
          spaceBetween: 50,
        },
      },

  });

  $('.controls .next').click(function(){
    recenze_slider.slideNext();
  })

  $('.controls .prev').click(function(){
    recenze_slider.slidePrev();
  })

})

if($(window).outerWidth() < 767){
  $(".online_publick_school_title").insertAfter(".guy_section2");
}

$(".open_menu").click(function(){
  $(".navmenu").slideToggle()
})

if($(window).outerWidth() < 767){
  $(".navmenu").insertAfter(".under_header");
}
